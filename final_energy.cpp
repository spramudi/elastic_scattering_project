#include <iostream>
#include <cmath>
using namespace std;

//---------------------------
//NEWTON ROOT FINDER 
//---------------------------
double newton(double A, double thetaL){
double f, fprime, error, thetaC_new, thetaC, tan_thetaL;
int iter;

tan_thetaL = tan(thetaL);
thetaC = M_PI/2;//initial guess of thetaC

do{
f = sin(thetaC) - tan_thetaL/A - tan_thetaL*cos(thetaC);
fprime = cos(thetaC) + tan_thetaL*sin(thetaC);

thetaC_new = thetaC - f/fprime;
error = abs((thetaC_new - thetaC)/thetaC);
thetaC = thetaC_new;

iter++;
}while((error>1e-6) && (iter<50));

return thetaC;
}


//---------------------------
//CALCULATE FINAL ENERGY AFTER COLLISION 
//---------------------------
double final_energy(double Ei, double A, double thetaL){
double beta, alpha, Ef,thetaC;
  
beta = (A-1)/(A+1);
alpha = pow(beta,2); 
thetaC = newton(A, thetaL);
  
Ef = 0.5 * Ei * ((1+alpha) + (1-alpha)*cos(thetaC));

return Ef;  
}


main(){
double Ef;

//cout <<"main";
Ef = final_energy(5, 1, 180*M_PI/180);
cout <<"Ef = "<<Ef<<"\n";

return 0;
}